package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired private ChildEntityRepository childEntityRepository;

    @Autowired private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        flushAndClear(
                em -> {
                    ParentEntity parent = new ParentEntity("Ocean");
                    ChildEntity child = new ChildEntity("Sea");
                    child.setParent(parent);
                    childEntityRepository.save(child);
                    parentEntityRepository.save(parent);
                    childId.setValue(child.getId());
                });

        run(
                em -> {
                    assertNotNull(
                            childEntityRepository.findById(childId.getValue()).get().getParent());
                });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();
        flushAndClear(
                em -> {
                    ParentEntity parent = new ParentEntity("Ocean");
                    ChildEntity child = new ChildEntity("Sea");
                    child.setParent(parent);
                    childEntityRepository.save(child);
                    parentEntityRepository.save(parent);
                    childId.setValue(child.getId());
                });

        flushAndClear(
                em -> {
                    ChildEntity child = childEntityRepository.findById(childId.getValue()).get();
                    child.setParent(null);
                });

        run(
                em -> {
                    assertNull(
                            childEntityRepository.findById(childId.getValue()).get().getParent());
                });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();
        flushAndClear(
                em -> {
                    ParentEntity parent = new ParentEntity("Ocean");
                    ChildEntity child = new ChildEntity("Sea");
                    child.setParent(parent);
                    childEntityRepository.save(child);
                    parentEntityRepository.save(parent);
                    parentId.setValue(parent.getId());
                    childId.setValue(child.getId());
                });

        flushAndClear(
                em -> {
                    ParentEntity parent =
                            parentEntityRepository.findById(parentId.getValue()).get();
                    ChildEntity child = childEntityRepository.findById(childId.getValue()).get();

                    parentEntityRepository.delete(parent);
                    childEntityRepository.delete(child);
                });

        run(
                em -> {
                    assertFalse(parentEntityRepository.findById(parentId.getValue()).isPresent());
                    assertFalse(childEntityRepository.findById(childId.getValue()).isPresent());
                });
        // --end-->
    }
}
