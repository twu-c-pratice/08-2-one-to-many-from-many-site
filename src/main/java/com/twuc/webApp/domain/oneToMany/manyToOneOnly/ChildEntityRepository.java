package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildEntityRepository extends JpaRepository<ChildEntity, Long> {
}
